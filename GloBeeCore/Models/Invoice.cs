﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace GloBeeCore.Models
{
    public class Invoice
    {
        public string Guid { get; set; }
        public long Nonce { get; set; }
        public string Token { get; set; }
        public double Price { get; set; }

        private string _currency = "";
        public string Currency
        {
            get { return _currency; }
            set
            {
                if (value.Length != 3)
                {
                    throw new Exception("Error: currency code must be exactly three characters");
                }
                _currency = value;
            }
        }

        // Optional fields
        public string OrderId { get; set; }
        public string ItemDesc { get; set; }
        public string ItemCode { get; set; }
        public string PosData { get; set; }
        [JsonProperty(PropertyName = "notificationURL")]
        public string NotificationUrl { get; set; }
        public string TransactionSpeed { get; set; }
        public bool FullNotifications { get; set; }
        public string NotificationEmail { get; set; }
        [JsonProperty(PropertyName = "redirectURL")]
        public string RedirectUrl { get; set; }
        public bool Physical { get; set; }
        public string BuyerName { get; set; }
        public string BuyerAddress1 { get; set; }
        public string BuyerAddress2 { get; set; }
        public string BuyerCity { get; set; }
        public string BuyerState { get; set; }
        public string BuyerZip { get; set; }
        public string BuyerCountry { get; set; }
        public string BuyerEmail { get; set; }
        public string BuyerPhone { get; set; }

        // Response fields
        [JsonIgnore]
        public ResponseData Response { get; set; }

        /// <summary>
        /// Creates a new Invoice model
        /// </summary>
        public Invoice()
        {
            
        }

        public class ResponseData
        {
            // Response fields
            public string Id { get; set; }
            public string Url { get; set; }
            public string Status { get; set; }
            public string Price { get; set; }
            public string PosData { get; set; }
            public double BtcPrice { get; set; }
            public long InvoiceTime { get; set; }
            public long ExpirationTime { get; set; }
            public long CurrentTime { get; set; }
            public string BitcoinAddress { get; set; }
            public string Token { get; set; }
            public string AltPrice { get; set; }
            public string AltCurrency { get; set; }
            public string AltCurrencyPaid { get; set; }
            //public List<InvoiceTransaction> Transactions { get; set; }
            public double? Rate { get; set; }
            //public Dictionary<string, string> ExRates { get; set; }
            public string ExceptionStatus { get; set; }
            //public InvoicePaymentUrls PaymentUrls { get; set; }
            public string RawJson { get; set; }
        }
    }
}
