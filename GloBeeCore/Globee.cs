﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using GloBeeCore.Models;
using GloBeeCore.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace GloBeeCore
{
    public class Globee
    {
        private Facade _facade;
        private readonly string _baseApiUrl = "https://test.globee.com/";
        private readonly string _baseApiVersion = "2.0.0";
        private readonly string _xIdentity;
        private readonly EcKey _ecKey;

        public Globee(bool isTest, string xIdentity, Facade facade, string ecKeyFilePath)
        {
            _xIdentity = xIdentity;
            _facade = facade;
            _ecKey = Utils.LoadEcKey(ecKeyFilePath);

            if (!isTest)
                _baseApiUrl = "https://globee.com/";
        }

        public Invoice CreateInvoice(Invoice invoice)
        {
            var json = JsonConvert.SerializeObject(
                invoice, 
                Formatting.None,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });

            var endpoint = "invoices"; // Endpoint to post to

            using (var hc = new HttpClient())
            {
                // Base address
                hc.BaseAddress = new Uri(_baseApiUrl);
                
                // Set headers
                hc.DefaultRequestHeaders.Clear();

                var signature = Utils.Sign(_ecKey, _baseApiUrl + endpoint + json);
                hc.DefaultRequestHeaders.Add("x-signature", signature);
                hc.DefaultRequestHeaders.Add("x-accept-version", _baseApiVersion);
                hc.DefaultRequestHeaders.Add("x-identity", _xIdentity);
                
                // Whole RAW body content in json
                var bodyContent = new StringContent(Utils.UnicodeToAscii(json));
                bodyContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                
                // Post to GloBee
                var result = hc.PostAsync(endpoint, bodyContent).Result;

                var body = result.Content.ReadAsStringAsync().Result;
                var j = JObject.Parse(body);
                var d = j.Value<JObject>("data");

                invoice.Response = d.ToObject<Invoice.ResponseData>();
                invoice.Response.RawJson = j.ToString(Formatting.None);

                return invoice;
            }
        }
    }
}
