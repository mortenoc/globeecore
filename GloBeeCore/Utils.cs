﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using GloBeeCore.Models;
using Newtonsoft.Json.Linq;
using Org.BouncyCastle.Crypto.Digests;

namespace GloBeeCore
{
    internal static class Utils
    {
        private static readonly char[] HexArray = "0123456789abcdef".ToCharArray();

        public static string UnicodeToAscii(string json)
        {
            var unicodeBytes = Encoding.Unicode.GetBytes(json);
            var asciiBytes = Encoding.Convert(Encoding.Unicode, Encoding.ASCII, unicodeBytes);
            var asciiChars = new char[Encoding.ASCII.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
            Encoding.ASCII.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
            return new string(asciiChars);
        }

        public static EcKey LoadEcKey(string localFilePath)
        {
            using (FileStream fs = File.OpenRead(localFilePath))
            {
                byte[] b = new byte[1024];
                fs.Read(b, 0, b.Length);
                EcKey key = EcKey.FromAsn1(b);
                return key;
            }
        }

        public static string Sign(EcKey ecKey, string input)
        {
            var hash = Sha256Hash(input);
            return BytesToHex(ecKey.Sign(HexToBytes(hash)));
        }

        private static string Sha256Hash(string value)
        {
            var sb = new StringBuilder();
            using (var hash = SHA256Managed.Create())
            {
                var enc = Encoding.UTF8;
                var result = hash.ComputeHash(enc.GetBytes(value));

                foreach (var b in result)
                    sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }

        public static byte[] HexToBytes(string hex)
        {
            if (hex == null)
                throw new ArgumentNullException("hex");
            if (hex.Length % 2 == 1)
                throw new FormatException("The binary key cannot have an odd number of digits");

            if (hex == string.Empty)
                return new byte[0];

            var arr = new byte[hex.Length >> 1];

            for (var i = 0; i < hex.Length >> 1; ++i)
            {
                var highNibble = hex[i << 1];
                var lowNibble = hex[(i << 1) + 1];

                if (!IsValidHexDigit(highNibble) || !IsValidHexDigit(lowNibble))
                    throw new FormatException("The binary key contains invalid chars.");

                arr[i] = (byte)((GetHexVal(highNibble) << 4) + (GetHexVal(lowNibble)));
            }
            return arr;
        }

        public static string BytesToHex(byte[] bytes)
        {
            var hexChars = new char[bytes.Length * 2];
            for ( var j = 0; j < bytes.Length; j++ ) {
                var v = bytes[j] & 0xFF;
                hexChars[j * 2] = HexArray[(int)((uint)v >> 4)];
                hexChars[j * 2 + 1] = HexArray[v & 0x0F];
            }
            return new string(hexChars);
        }

        private static int GetHexVal(char hex)
        {
            var val = (int)hex;
            return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }
        private static bool IsValidHexDigit(char chr)
        {
            return ('0' <= chr && chr <= '9') || ('a' <= chr && chr <= 'f') || ('A' <= chr && chr <= 'F');
        }

        /// <summary>
        /// Returns the given byte array hex encoded.
        /// </summary>
        public static string BytesToHexString(byte[] bytes)
        {
            var buf = new StringBuilder(bytes.Length*2);
            foreach (var b in bytes)
            {
                var s = b.ToString("x");
                if (s.Length < 2)
                    buf.Append('0');
                buf.Append(s);
            }
            return buf.ToString();
        }

        /// <summary>
        /// Calculates RIPEMD160(SHA256(input)). This is used in Address calculations.
        /// </summary>
        public static byte[] Sha256Hash160(byte[] input)
        {
            var sha256 = new SHA256Managed().ComputeHash(input);
            var digest = new RipeMD160Digest();
            digest.BlockUpdate(sha256, 0, sha256.Length);
            var @out = new byte[20];
            digest.DoFinal(@out, 0);
            return @out;
        }
    }
}
